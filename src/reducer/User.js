import {FETCH_USER} from '../constants/ActionType'
const initialState = {
  user: {}
}

export const User = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USER:
      return {
        ...state,
        user: action.payload
      };
    default:
      return state;
  }
}