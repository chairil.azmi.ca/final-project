import {FETCH_CATEGORY, FETCH_DETAIL_PRODUCT, FETCH_PRODUCTS} from '../constants/ActionType'
const initialState = {
  category: [],
  products: [],
  detail: []
}

export const Category = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CATEGORY:
      return {
        ...state,
        category: action.payload
      };
      case FETCH_PRODUCTS:
      return {
        ...state,
        products: action.payload
      };
      case FETCH_DETAIL_PRODUCT:
      return {
        ...state,
        detail: action.payload
      };
  
    default:
      return state;
  }
}