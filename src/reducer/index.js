import { combineReducers } from 'redux'

import {Login} from './Auth'
import {Category} from './Product'
import {User} from './User'

const rootReducer = combineReducers({
  login: Login,
  category: Category,
  user: User
})

export default rootReducer

