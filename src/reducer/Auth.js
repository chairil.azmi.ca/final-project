import {AUTH_LOGIN, AUTH_LOGOUT} from '../constants/ActionType'
const initialState = {
  user: {},
  isLogin: false
};

export const Login = (state = initialState, action) => {
  switch (action.type) {
    case AUTH_LOGIN:
      // console.log(action)
      return {
        ...state,
        isLogin: true
      };
    case AUTH_LOGOUT:
      return {
        ...state,
        isLogin: false
      }
    default:
      return state;
  }
}