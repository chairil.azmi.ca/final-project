import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, AsyncStorage } from 'react-native';
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
  
} from '@react-navigation/drawer'
import { MaterialCommunityIcons } from '@expo/vector-icons'

import Login from '../screen/Login'
import Register from '../screen/Register'
import Main from '../screen/Main'
import Home from '../screen/Home'
import Products from '../screen/Products'
import DetailProduct from '../screen/DetailProduk'
import Profile from '../screen/DataUser'

import {BASE_URL} from '../src/constants/Api'

import { connect } from 'react-redux';
import Axios from 'axios';
import { FETCH_CATEGORY, AUTH_LOGIN, AUTH_LOGOUT, FETCH_USER } from '../src/constants/ActionType';


const RootStack = createStackNavigator();
const Drawer = createDrawerNavigator()
const HomeStack = createStackNavigator()

const HomeStackScreen = (props) => {
  console.log(props)
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen name="Home" component={Home} options={
        {
          title: 'Home',
          headerLeft: () => {
            return (
              <MaterialCommunityIcons.Button 
                name="menu" 
                color="#000000"
                backgroundColor="#ffffff"
                onPress={
                  () => {
                    props.navigation.openDrawer()
                  }
                }
              />
            )
          }
        }
      }></HomeStack.Screen>
      <HomeStack.Screen name="Products" component={Products} options={() => ({title: 'Products'})}></HomeStack.Screen>
      <HomeStack.Screen name="DetailProducts" component={DetailProduct} options={({route}) => ({title: route.params})}></HomeStack.Screen>
    </HomeStack.Navigator>
  )
}

const DrawerNavigator = () => {
  return (
    <Drawer.Navigator drawerContent={props => <CustomDrawerContent {...props}/>}>
      <Drawer.Screen name="Home" component={HomeStackScreen}/>
      <Drawer.Screen name="Profile" component={Profile}/>
    </Drawer.Navigator>
  )
}

const CustomDrawerContent = (props) => {
  // console.log(props)
  return (
    <DrawerContentScrollView {...props}>
      <DrawerItemList {...props} />
      <DrawerItem label="Logout" onPress={() => logout()} />
    </DrawerContentScrollView>
  )
}

const logout = async () => {
  try {
    await AsyncStorage.clear(err => {
      if(!err){
        _props.logout()
      }
    })
  } catch (err) {
    console.log(err)
  }
}

let _props = {}

const fetcCategory = async () => {
  try {
    const res = await Axios.get(BASE_URL + 'category')
    if(res){
      // console.log(res.data)
      _props.category(res.data)

    }
  } catch (err) {
    console.log(err)
  }
}

const fetchProfile = async () => {
  try {
    if(getId){
      const res = await Axios.get(BASE_URL + 'customer/' + await AsyncStorage.getItem('id'))
      _props.user(res.data)
    }
  } catch (err) {
    console.log(err)
  }
}

const getId = async () => {
  try{
    const id = await AsyncStorage.getItem('id')
    return id
  } catch (err) {
    console.log(err)
    return null
  }
}
 
class AppNavigation extends React.Component {

  constructor(props){
    super(props)
    _props = {...props}
  }

  async getLocalStorage(){
    const id = await AsyncStorage.getItem('id')
    if(id !== null) {
      this.props.login()
    }
  }

  componentDidMount() {
    this.getLocalStorage()
    fetcCategory()
    fetchProfile()
  }
  
  render(){
    const isLogin = false
    return (
      <NavigationContainer>
        <RootStack.Navigator>
          {!this.props.isLogin?(
            <>
              <RootStack.Screen name="Main" component={Main} options={{headerShown: false}}></RootStack.Screen>
              <RootStack.Screen name="Login" component={Login}></RootStack.Screen>
              <RootStack.Screen name="Register" component={Register}></RootStack.Screen>
            </>
          ) : (
            <>
              {/* <RootStack.Screen name="Home" component={Home} ></RootStack.Screen>
              <RootStack.Screen name="Products" component={Products}></RootStack.Screen> */}
              <RootStack.Screen name="Home" component={DrawerNavigator} options={{headerShown: false}}></RootStack.Screen>
            </>
          )}
          
        </RootStack.Navigator>      
      </NavigationContainer>
    );

  }
}

const mapStateToProps = (state) => {
  return {
    isLogin: state.login.isLogin
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    login: () => dispatch({
      type: AUTH_LOGIN
    }),
    logout: () => dispatch({
      type: AUTH_LOGOUT
    }),
    category: (data) => dispatch({
      type: FETCH_CATEGORY,
      payload: data
    }),
    user: (data) => dispatch({
      type: FETCH_USER,
      payload: data
    })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AppNavigation)


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
