import { StatusBar } from 'expo-status-bar';
import React from 'react';
import AppNavigation from './Navigation'
import { Provider } from 'react-redux';
import store from '../src/store/store';

 
export default App = () => {
  return (
    <Provider store={store}>
      <AppNavigation></AppNavigation>
    </Provider>
  );
}
