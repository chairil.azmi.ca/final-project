import { StatusBar } from 'expo-status-bar';
import { useFonts, SairaCondensed_500Medium } from '@expo-google-fonts/saira-condensed'
import React from 'react';
import { StyleSheet, 
  Text, 
  Image,
  TouchableOpacity,
  View, 
  Dimensions,
  ImageBackground,
  Button
} from 'react-native';
import { FlatList } from 'react-native-gesture-handler';

const DEVICE = Dimensions.get('window')

const ITemView = () => {
  return (
    <View style={Styles.itemViewContainer}>
      <View style={{alignItems: 'center'}}>
        <Image style={{width: 104, height: 104}} source={require('../assets/images/apron1.png')}/>
      </View>
      <View style={{alignItems: 'center'}}> 
        <Text>Apron Drill Celemek</Text>
      </View>
      <View style={{alignItems: 'center'}}>
        <Text>Rp 55.000</Text>
      </View>
      <View style={{alignItems: 'center'}}>
        <TouchableOpacity style={Styles.button}>
          <Text style={Styles.buttonText}>BELI</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default (props) => {
  console.log(props)
  let [fontsLoaded] = useFonts({
    SairaCondensed_500Medium,
  });

  if (!fontsLoaded) {
    return <Text>Loading...</Text>;
  }
  return (
    <View style={Styles.container}>
      <View style={{alignItems: 'flex-end', marginHorizontal: 15, marginTop: 5,marginBottom: 40}}>
        <Text style={Styles.textTitle}>Alat-Alat Dapur</Text>
      </View>
      <View>
        <TouchableOpacity>
          <ITemView/>
        </TouchableOpacity>
      </View>
    </View>
  )
}

const Styles = StyleSheet.create({
  container: {
    flex: 1, 
    flexDirection: 'column',
    padding: 10,
    backgroundColor: '#FFFFFF'
  },
  textTitle: {
    fontFamily: 'SairaCondensed_500Medium',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize:30,
    lineHeight: 47
  },
  itemViewContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 140,
    height: 210,
    backgroundColor: '#FFF1F1',
    borderWidth: 4,
    borderColor: '#000000',
    borderStyle: 'solid'
  },
  productTitle: {
    fontFamily: 'SairaCondensed_500Medium',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize:12,
    lineHeight: 19
  },
  productPrice: {
    fontFamily: 'SairaCondensed_500Medium',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize:10,
    lineHeight: 16
  },
  button: {
    width: 50,
    height: 30,
    backgroundColor: '#000000',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    fontFamily: 'SairaCondensed_500Medium',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 10,
    lineHeight: 14,
    color: '#FFFFFF'
  }
})