import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, 
  Text, 
  View,
  Image,
  ImageBackground, 
  Dimensions,
  TextInput,
  TouchableOpacity
} from 'react-native';

import { useFonts, SairaCondensed_500Medium } from '@expo-google-fonts/saira-condensed'
import Axios from 'axios';
import { BASE_URL } from '../src/constants/Api'

const DEVICE = Dimensions.get('window')

const register = async (form) => {
  try{ 
    const res = await Axios.post(BASE_URL + '/register', form)
    this.setState({user: res.data})
    console.log(res.data)
  } catch(err) {
    console.log(err)
  }
    // console.log(form)
  // Axios.post(BASE_URL + 'register', form).then(res => {
  //   if(res) {
  //     // this.setState({user: res.data})
  //   console.log(res.data)
  //   }
  // }).catch(err => {
  //   console.log(err)
  // })
}

export default class Register extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      user: {},
      form: {
        username: '',
        email: '',
        password: ''
      }
    }
  }

  render() {
    // let [fontsLoaded] = useFonts({
    //   SairaCondensed_500Medium,
    // });
    
    // if (!fontsLoaded) {
    //   return <Text>Loading...</Text>;
    // }
    
    return (
      <View style={styles.container}>
        <ImageBackground style={{height: DEVICE.height, width: DEVICE.width}} source={require('../assets/images/Vector3.png')}>
          <Image style={styles.logo} source={require('../assets/images/stiker1.png')}></Image>
          <View style={styles.formContainer}>
            <View style={{marginBottom: 5}}>
              <Text style={styles.textInputTitle}>Username</Text>
              <TextInput style={styles.textInput} 
                placeholder="Ketik Username Disini" 
                placeholderTextColor="#FFFFFF"
                value={this.state.form.username}
                onChangeText={(v) => {
                  const form = {...this.state.form}
                  form.username = v
                  this.setState({form})}}
              ></TextInput>
            </View>
            <View style={{marginBottom: 5}}>
              <Text style={styles.textInputTitle}>Email</Text>
              <TextInput style={styles.textInput} 
                placeholder="Ketik Email Disini" 
                placeholderTextColor="#FFFFFF"
                value={this.state.form.email}
                onChangeText={(v) => { 
                  const form = {...this.state.form}
                  form.email = v
                  this.setState({form})
                }}
              ></TextInput>
            </View>
            <View style={{marginBottom: 5}}>
              <Text style={styles.textInputTitle}>Password</Text>
              <TextInput style={styles.textInput} 
                placeholder="Ketik Password Disini" 
                placeholderTextColor="#FFFFFF" 
                secureTextEntry={true}
                value={this.state.form.password}
                onChangeText={(v) => {
                  const form = {...this.state.form}
                  form.password = v
                  this.setState({form})
                }}
              ></TextInput>
            </View>
            <TouchableOpacity style={styles.button} onPress={() => register(this.state.form)}>
              <Text style={{color: 'white', fontFamily: 'SairaCondensed_500Medium'}}>Register</Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    top: DEVICE.height * 0.8,
    left: DEVICE.width * 0.75,
    width: 61,
    height: 54
  },
  formContainer: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    padding: 10,
    marginTop: DEVICE.height * 0.1
  },
  textInput: {
    width: DEVICE.width * 0.8,
    height: 40,
    padding: 5,
    backgroundColor: '#E9E0D6',
    borderWidth: 3,
    borderColor: '#000000',
    borderStyle: "solid",
    shadowOffset: {
      width: 2,
      height: 0
   },
   shadowOpacity: 1,
   shadowColor: '#000000',
   shadowRadius: 10,
  },
  button: {
    alignSelf: 'flex-start',
    alignItems: 'center',
    justifyContent: 'center',
    height: 40,
    width: DEVICE.width * 0.3,
    backgroundColor: '#8C1515',
    padding: 10,
    marginTop: 70,
    left: DEVICE.width * 0.07,
    borderRadius: 7,
    shadowOffset:{
      width: 0,
      height: 4
    },
    shadowColor: 'rgba(0, 0, 0, 0.25)',
    elevation: 7
  },
  textInputTitle: {
    fontFamily: 'SairaCondensed_500Medium',
    fontSize: 18,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 28,
    color: '#000000'
  }
});
