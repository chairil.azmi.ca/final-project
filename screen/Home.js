import { StatusBar } from 'expo-status-bar';
import { useFonts, SairaCondensed_500Medium } from '@expo-google-fonts/saira-condensed'
import React from 'react';
import { StyleSheet, 
  Text, 
  Image,
  TouchableOpacity,
  View, 
  Dimensions,
  ImageBackground
} from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { connect } from 'react-redux';

const DEVICE = Dimensions.get('window')

const ItemView = (props) => {
  const data = props.data
  return (
    <View style={{marginBottom: 30}}>
      <View style={styles.itemConstructor}>
        <Image style={{width: 117, height: 90}} source={data.item.image? {uri: data.item.image} : require('../assets/images/11.png')}/>
      </View>
      <View style={{alignItems: 'center', justifyContent: 'center'}}>
        <Text style={styles.categoryText}>{data.item.name ? data.item.name : 'No Name'}</Text>
      </View>
    </View>
    
  )
}

const Home = (props) => {
  // console.log(props.category)
  let [fontsLoaded] = useFonts({
    SairaCondensed_500Medium,
  });

  if (!fontsLoaded) {
    return <Text>Loading...</Text>;
  }
  return (
    <View style={styles.container}>
      {/* <View style={{left: 0, flex: 1,}}>
       <ImageBackground style={{height: DEVICE.height, width: 65}} source={require('../assets/images/Vector4.png')}/>
      </View> */}
      <View style={{alignItems: 'flex-end', marginHorizontal: 15, marginTop: 5,marginBottom: 5}}>
        <Text style={styles.textTitle}>Kategori Barang</Text>
      </View>
      <View style={styles.listConstructor}>
        <FlatList 
          style={{height: DEVICE.height * 0.75}}
          data={props.category}
          keyExtractor={(item) => item.id.toString()}
          renderItem={(item) => <TouchableOpacity onPress={()=> props.navigation.push('Products', item.item.products)}><ItemView data={item}></ItemView></TouchableOpacity>}
        >

        </FlatList>
      </View>
    </View>
  )
}

const mapStateToProps = (state) => {
  return {
    category: state.category.category
  }
} 

export default connect(mapStateToProps)(Home)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start'
  },
  textTitle: {
    fontFamily: 'SairaCondensed_500Medium',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize:30,
    lineHeight: 47
  },
  listConstructor: {
    // flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  itemConstructor: {
    width: 294,
    height: 119,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    borderColor: '#000000',
    borderRadius: 15,
    borderWidth: 4,
  },
  categoryText: {
    fontFamily: 'SairaCondensed_500Medium',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 18,
    lineHeight: 28
  }
})