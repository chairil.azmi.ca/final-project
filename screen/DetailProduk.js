import React,{Component} from 'react';
import{
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    ImageBackground,
    Dimensions,
    
} from 'react-native'
// import {AuthContext}
import Icon from "react-native-vector-icons/MaterialIcons";
import { TouchableOpacity } from 'react-native-gesture-handler';
const DEVICE = Dimensions.get('window')

export default class AboutScreen extends Component{
    currencyFormat(num) {
        return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    };
    render(){
        return(
            <View style={styles.container} key={this.props.route.params.name}>
             <ImageBackground style={{height: DEVICE.height, width: DEVICE.width}} source={require('../assets/images/Vector3.png')}>
                <View style={styles.navbar}>
                    {/* <Icon name="arrow-back" size={25} oonPress={()=>{this.props.navigation.goBack()}}></Icon> */}
                </View>
                <View style={styles.displayPicture}>
                    <Image source={{uri: this.props.route.params.image}} style={{height: 200, width: 200}}></Image>
                    <Text style={{fontSize:20,fontFamily:'Roboto', marginTop:10}}>{this.props.route.params.name}</Text>
                    <Text style={{fontSize:20,fontFamily:'Roboto', marginTop:10,}}>Harga</Text>
                    <Text style={{fontSize:20,fontFamily:'Roboto', marginTop:10, color:'red',backgroundColor:'white'}}>{this.currencyFormat(this.props.route.params.cash)}</Text>
                    <Text style={{fontSize:20,fontFamily:'Roboto', marginTop:30, alignSelf: 'flex-start', marginLeft:10}}>Deskripsi :</Text>
                    <Text style={{fontSize:20,fontFamily:'Roboto', marginTop:10, alignSelf: 'flex-start', marginLeft:10}}>{this.props.route.params.description}</Text>
                </View>
                <TouchableOpacity>
                <Text style={{fontSize:20,fontFamily:'Roboto', marginTop:10, backgroundColor: '#ffffff', textAlign:'center'}}>Beli</Text>
                </TouchableOpacity>
             </ImageBackground>
            </View>
        )
    }
}

const styles=StyleSheet.create({
    container:{
        flex:1
    },
    navbar:{
        margin:25,
    },
    displayPicture:{
        alignItems:'center'
    }
})
