import React,{Component} from 'react';
import{
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    ImageBackground,
    Dimensions,
    
} from 'react-native'
// import {AuthContext}
import {MaterialCommunityIcons} from "@expo/vector-icons";
import Icon from "react-native-vector-icons/MaterialIcons";
import { connect, Provider } from 'react-redux';
// import { Icon } from 'react-native-paper/lib/typescript/src/components/Avatar/Avatar';
const DEVICE = Dimensions.get('window')

class AboutScreen extends Component{
    render(){
        const user = this.props.user.user
        console.log(user.username)
        return(
            <View style={styles.container}>
             <ImageBackground style={{height: DEVICE.height, width: DEVICE.width}} source={require('../assets/images/Vector3.png')}>
                <View style={styles.navbar}>
                    <Icon name="arrow-back" size={25} onPress={()=>{this.props.navigation.goBack()}}></Icon>
                </View>
                <View style={styles.displayPicture}>
                    {/* <Image source={require("../assets/images/Profil.png")}></Image> */}
                    <MaterialCommunityIcons name="account-circle" size={150}></MaterialCommunityIcons>
                </View>
                <View style={{marginTop: 60, alignContent: 'flex-start'}}>
                    <Text style={{fontSize:20,fontFamily:'Roboto', alignSelf: 'flex-start', marginLeft: 10}}>Username:</Text>
                    <Text style={{fontSize:20,fontFamily:'Roboto', alignSelf: 'flex-start', marginLeft: 10, marginBottom: 15}}>{user.username}</Text>
                    <Text style={{fontSize:20,fontFamily:'Roboto', alignSelf: 'flex-start', marginLeft: 10}}>Email:</Text>
                    <Text style={{fontSize:20,fontFamily:'Roboto', alignSelf: 'flex-start', marginLeft: 10}}>{user.email}</Text>
                </View>
             </ImageBackground>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps)(AboutScreen)

const styles=StyleSheet.create({
    container:{
        flex:1
    },
    navbar:{
        margin:25,
    },
    displayPicture:{
        alignItems:'center'
    }
})
