import { StatusBar } from 'expo-status-bar';
import { useFonts, SairaCondensed_500Medium } from '@expo-google-fonts/saira-condensed'
import React from 'react';
import { StyleSheet, 
  Text, 
  Image,
  TouchableOpacity,
  View, 
  Dimensions
} from 'react-native';

const DEVICE = Dimensions.get('window')

export default ({navigation}) => {
  let [fontsLoaded] = useFonts({
    SairaCondensed_500Medium,
  });

  if (!fontsLoaded) {
    return <Text>Loading...</Text>;
  }

  return (
    <View style={styles.container}>
      <View style={styles.logo}> 
        <Image source={require('../assets/images/stiker1.png')}></Image>
      </View>
      <View>
        <Text style={styles.text}>
          Hai
        </Text>
      </View>
      <View style={{marginLeft: 25}}>
        <Text style={styles.text}>
          Come Here
        </Text>
      </View>
      <View>
        <Text style={styles.text}>
          Get your dream item here!
        </Text>
        <View>
        <View style={{marginVertical: 30}}>
          <View style={{marginVertical: 20, alignItems: 'flex-end'}}>
            <TouchableOpacity style={styles.button} onPress={() => {navigation.push('Login')}}>
              <Text style={{color: 'white', fontFamily: 'SairaCondensed_500Medium'}}>Login</Text>
            </TouchableOpacity>
          </View>
          <View style={{marginVertical: 20}}>
            <TouchableOpacity style={styles.button} onPress={() => {navigation.push('Register')}}>
              <Text style={{color: 'white', fontFamily: 'SairaCondensed_500Medium'}}>Register</Text>
            </TouchableOpacity>
          </View>
        </View>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 10
  },
  logo: {
    height: DEVICE.width * 0.5,
    alignItems: 'center', 
    justifyContent: 'center',
    marginTop: 65
  },
  text: {
    fontFamily: 'SairaCondensed_500Medium',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 24,
    lineHeight: 38,
    color: '#000000',
    textShadowColor: 'rgba(0, 0, 0, 0.35)',
    textShadowOffset: {
      width: 0,
      height: 8
    },
    textShadowRadius: 10,
  },
  button: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    height: 40,
    width: DEVICE.width * 0.6,
    backgroundColor: '#8C1515',
    padding: 10,
    borderRadius: 7,
    shadowOffset:{
      width: 0,
      height: 4
    },
    shadowColor: 'rgba(0, 0, 0, 0.25)',
    elevation: 7
  },
});
