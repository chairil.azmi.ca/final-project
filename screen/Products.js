import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, TextInput, Button } from 'react-native';

// import data from './data.json';

const DEVICE = Dimensions.get('window')

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      searchText: '',
      totalPrice: 0,
    }
  }

  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  };

  updatePrice(price) {
    let addPrice = parseInt(price)
    let total = this.state.totalPrice + addPrice
    this.setState({totalPrice: total})
  }


  render() {
    return (
      <View style={styles.container}>
        <View style={{ minHeight: 50, width: DEVICE.width * 0.88 + 30, marginVertical: 5 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text>Hai{'\n'}
              <Text style={styles.headerText}>{this.props.route.params.userName}</Text>{'\n'}
            <Text>Selamat Belanja</Text>
            </Text>
            <Text style={{ textAlign: 'right' }}>Total Harga{'\n'}
              <Text style={styles.headerText}>{this.currencyFormat(this.state.totalPrice)}</Text>
            </Text>
          </View>
          <View>
          </View>
          <TextInput
            style={{ backgroundColor: 'white', marginTop: 8 }}
            placeholder=' Cari barang..'
            onChangeText={(searchText => this.setState({ searchText }))}
          />
        </View>

        <FlatList 
          data={this.props.route.params}
          numColumns={2}
          keyExtractor={(data) => data.id.toString()}
          renderItem={(item) =>
            <TouchableOpacity onPress={() => this.props.navigation.push('DetailProducts', item.item)}>
            <ListItem data={item.item} 
                onPress={() => this.updatePrice(item.item.cash)}
            />
            </TouchableOpacity> 
          }
          />
      </View>
    )
  }
};

class ListItem extends React.Component {

  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  };

  render() {
    const data = this.props.data
    return (
      <View style={styles.itemContainer}>
        <Image source={{ uri: data.image }} style={styles.itemImage} resizeMode='contain' />
        <Text numberOfLines={2} ellipsizeMode='tail' style={styles.itemName} >{data.name}</Text>
        <Text style={styles.itemPrice}>{this.currencyFormat(Number(data.cash))}</Text>
        <Button title='BELI' color='black' onPress={this.props.onPress}/>
      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    fontSize: 20,
    fontWeight: 'bold'
  },

  //? Lanjutkan styling di sini
  itemContainer: {
    width: DEVICE.width * 0.44,
    margin: 8,
    padding: 10,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#FFF8DC"
  },
  itemImage: {
    flex: 1,
    width: 100,
    height: 100
  },
  itemName: {
    fontWeight: "bold",
    marginTop: 5,
    marginBottom: 10
  },
  itemPrice: {
    color: "blue",
    fontWeight: "bold",
    marginBottom: 5
  },
  itemStock: {
    marginBottom: 5
  },
  itemButton: {
    marginBottom: 5,
  },
  buttonText: {
    alignSelf: "center"
  }
})
