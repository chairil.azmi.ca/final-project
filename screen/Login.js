import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, 
  Text, 
  View,
  Image,
  ImageBackground, 
  Dimensions,
  TextInput,
  TouchableOpacity,
  AsyncStorage
} from 'react-native';
import Axios from 'axios';

import { useFonts, SairaCondensed_500Medium } from '@expo-google-fonts/saira-condensed'
import { connect } from 'react-redux';
import { Login } from '../src/reducer/Auth';
import { BASE_URL } from '../src/constants/Api';

const DEVICE = Dimensions.get('window')

const submit = async (email, password, dispatch) => {
  const form = {}
  form.email = email,
  form.password = password
  try{
    const res = await Axios.post(BASE_URL + 'login', form)
    if(res.data) {
      // await AsyncStorage.clear()    
      await AsyncStorage.setItem('id', res.data.id.toString())
      dispatch.login()
    }
    // console.log(res)
  } catch(err) {
    console.log(err)
  }
}

const LoginScreen = (props) => {
  const [email, setEmail] = React.useState('')
  const [password, setPassword] = React.useState('')

  let [fontsLoaded] = useFonts({
    SairaCondensed_500Medium,
  });
  
  if (!fontsLoaded) {
    return <Text>Loading...</Text>;
  }
  
  return (
    <View style={styles.container}>
      <ImageBackground style={{height: DEVICE.height, width: DEVICE.width}} source={require('../assets/images/Vector3.png')}>
        <Image style={styles.logo} source={require('../assets/images/stiker1.png')}></Image>
        <View style={styles.formContainer}>
          <View style={{marginBottom: 5}}>
            <Text style={styles.textInputTitle}>Username/Email</Text>
            <TextInput 
              style={styles.textInput} 
              placeholder="Ketik Username / Email Disini" 
              placeholderTextColor="#FFFFFF"
              value={email}
              onChangeText={(v) => { 
               setEmail(v)
              }}
            ></TextInput>
          </View>
          <View style={{marginBottom: 5}}>
            <Text style={styles.textInputTitle}>Password</Text>
            <TextInput style={styles.textInput} 
              placeholder="Ketik Password Disini" 
              placeholderTextColor="#FFFFFF" 
              secureTextEntry={true}
                
              value={password}
              onChangeText={(v) => { 
                setPassword(v)
              }}
              ></TextInput>
          </View>
          <TouchableOpacity style={styles.button} onPress={() => submit(email, password, props)}>
            <Text style={{color: 'white', fontFamily: 'SairaCondensed_500Medium'}}>Login</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </View>
  )
}

const mapDispatchtoProps = (dispatch) => {
  return {
    login: () => dispatch({
      type: 'AUTH_LOGIN'
    })
  }
}

export default connect(null, mapDispatchtoProps)(LoginScreen)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    top: DEVICE.height * 0.8,
    left: DEVICE.width * 0.75,
    width: 61,
    height: 54
  },
  formContainer: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10
  },
  textInput: {
    width: DEVICE.width * 0.8,
    height: 40,
    padding: 5,
    backgroundColor: '#E9E0D6',
    borderWidth: 3,
    borderColor: '#000000',
    borderStyle: "solid",
    shadowOffset: {
      width: 2,
      height: 0
   },
   shadowOpacity: 1,
   shadowColor: '#000000',
   shadowRadius: 10,
  },
  button: {
    alignSelf: 'flex-start',
    alignItems: 'center',
    justifyContent: 'center',
    height: 40,
    width: DEVICE.width * 0.3,
    backgroundColor: '#8C1515',
    padding: 10,
    marginTop: 70,
    left: DEVICE.width * 0.07,
    borderRadius: 7,
    shadowOffset:{
      width: 0,
      height: 4
    },
    shadowColor: 'rgba(0, 0, 0, 0.25)',
    elevation: 7
  },
  textInputTitle: {
    fontFamily: 'SairaCondensed_500Medium',
    fontSize: 18,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 28,
    color: '#000000'
  }
});
